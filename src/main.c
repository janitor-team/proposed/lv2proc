/*
 * NASPRO - The NASPRO Architecture for Sound PROcessing
 * A simple command line effect processor using LV2 plugins
 *
 * Copyright (C) 2011-2014 Stefano D'Angelo
 *
 * See the COPYING file for license conditions.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <locale.h>
#include <inttypes.h>
#include <stdbool.h>
#include <time.h>

#include <math.h>

#include <sndfile.h>

#include <lilv/lilv.h>

#include "config.h"

/* Utils */

static const char *argv0;

#define PRINT_ERROR(fmt) \
	fprintf(stderr, "%s: error: " fmt "\n", argv0);
#define PRINTF_ERROR(fmt, ...) \
	fprintf(stderr, "%s: error: " fmt "\n", argv0, __VA_ARGS__);
#define PRINT_WARNING(fmt) \
	fprintf(stderr, "%s: warning: " fmt "\n", argv0);
#define PRINTF_WARNING(fmt, ...) \
	fprintf(stderr, "%s: warning: " fmt "\n", argv0, __VA_ARGS__);

#define MIN(a, b) (((a) < (b)) ? (a) : (b))

static bool
strtoul_nl(unsigned long *v, const char *s)
{
	char *c, *locale;
	int err, errsv;

	errsv = errno;

	locale = setlocale(LC_NUMERIC, NULL);
	setlocale(LC_NUMERIC, "C");

	errno = 0;
	*v = strtoul(s, &c, 10);
	err = errno;

	setlocale(LC_NUMERIC, locale);

	errno = (err == 0) ? errsv : err;

	return (err == 0) && (*c == '\0');
}

static bool
strtof_nl(float *v, const char *s)
{
	char *c, *locale;
	int err, errsv;

	errsv = errno;

	locale = setlocale(LC_NUMERIC, NULL);
	setlocale(LC_NUMERIC, "C");

	errno = 0;
	*v = strtof(s, &c);
	err = errno;

	setlocale(LC_NUMERIC, locale);

	errno = (err == 0) ? errsv : err;

	return (err == 0) && (*c == '\0');
}

static bool
id_is_valid(const char *id)
{
	const char *c = id;

	if (((*c < 'A') || ((*c > 'Z') && (*c < 'a'))
	     || (*c > 'z')) && (*c != '_'))
		return 0;

	for (; *c != '\0'; c++)
		if (((*c < '0') || ((*c > '9') && (*c < 'A'))
		    || ((*c > 'Z') && (*c < 'a'))
		     || (*c > 'z')) && (*c != '_'))
			return 0;

	return 1;
}

/* Command line arguments */

#define FRAMES_N_DEFAULT 512

typedef struct
  {
	char		*symbol;
	float		 value;
	const LilvPort	*port;
  }
connection_t;

static const char *in_filename		= NULL;
static const char *out_filename		= NULL;

static unsigned long frames_n		= 0;	// 0 == unspecified

static connection_t *connections	= NULL;
static size_t connections_n		= 0;

static bool latency_do			= true;

static bool timing_do			= false;

static bool normalize_do		= false;

static char *plugin_uri			= NULL;

static connection_t *
cmd_connection_get_by_symbol(const char *symbol)
{
	if (connections == NULL)
		return NULL;

	for (size_t i = 0; i < connections_n; i++)
		if (!strcmp(connections[i].symbol, symbol))
			return connections + i;

	return NULL;
}

static connection_t *
cmd_connection_get_by_port(const LilvPort *port)
{
	for (size_t i = 0; i < connections_n; i++)
		if (connections[i].port == port)
			return connections + i;

	return NULL;
}

static int
cmd_parse(int argc, char *argv[])
{
	for (int i = 1; i < argc; i++)
		if (!strcmp(argv[i], "-h") || !strcmp(argv[i], "--help"))
		  {
			int argv0_len = strlen(argv[0]);

			printf("Usage: %s [options] -i input -o output "
			       "plugin\n", argv[0]);
			printf("       %*s -h|--help\n", argv0_len, " ");
			printf("       %*s --version\n", argv0_len, " ");
			printf("Command line arguments:\n");
			printf("  -h, --help         "
			       "Display this information\n");
			printf("  --version          "
			       "Display version information\n");
			printf("  -i input           "
			       "Input sound file\n");
			printf("  -o output          "
			       "Output sound file\n");
			printf("  plugin             "
			       "LV2 plugin URI\n");
			printf("  -n frames          "
			       "Number of frames per cycle [default=%d]\n",
			       FRAMES_N_DEFAULT);
			printf("  -c port:value      "
			       "Set value for control in port\n");
			printf("  --with-latency     "
			       "Disable latency compensation\n");
			printf("  --timing           "
			       "Report timing statistics\n");
			printf("  --normalize        "
			       "Normalize output\n");

			return 1;
		  }

	for (int i = 1; i < argc; i++)
		if (!strcmp(argv[i], "--version"))
		  {
			printf(PACKAGE " (" PACKAGE_NAME ") " VERSION "\n");
			printf("Copyright (C) 2011-2014 Stefano D'Angelo\n");
			printf("This is free software; see the source for "
			       "copying conditions.  There is NO\n");
			printf("warranty; not even for MERCHANTABILITY or "
			       "FITNESS FOR A PARTICULAR PURPOSE.\n\n");

			return 1;
		  }

	for (int i = 1; i < argc; i++)
	  {
		if (!strcmp(argv[i], "-i"))
		  {
			if (i == (argc - 1))
			  {
				PRINT_ERROR("missing filename after '-i'");
				goto cmd_parse_err;
			  }

			if (in_filename != NULL)
			  {
				PRINT_ERROR("input file specified more than "
					    "once");
				goto cmd_parse_err;
			  }

			in_filename = argv[++i];
			continue;
		  }

		if (!strcmp(argv[i], "-o"))
		  {
			if (i == (argc - 1))
			  {
				PRINT_ERROR("missing filename after '-o'");
				goto cmd_parse_err;
			  }

			if (out_filename != NULL)
			  {
				PRINT_ERROR("output file specified more than "
					    "once");
				goto cmd_parse_err;
			  }

			out_filename = argv[++i];
			continue;
		  }

		if (!strcmp(argv[i], "-n"))
		  {
			if (i == (argc - 1))
			  {
				PRINT_ERROR("missing number of frames after "
					    "'-n'");
				goto cmd_parse_err;
			  }

			if (frames_n != 0)
			  {
				PRINT_ERROR("number of frames specified more "
					    "than once");
				goto cmd_parse_err;
			  }

			bool err = !strtoul_nl(&frames_n, argv[++i]);
			if (err || (frames_n == 0))
			  {
				PRINT_ERROR("invalid number of frames");
				goto cmd_parse_err;
			  }

			continue;
		  }

		if (!strcmp(argv[i], "-c"))
		  {
			if (i == (argc - 1))
			  {
				PRINT_ERROR("missing <port>:<value> after "
					    "'-c'");
				goto cmd_parse_err;
			  }

			char *v = strchr(argv[++i], ':');
			if (v == NULL)
			  {
				PRINT_ERROR("invalid <port>:<value> argument");
				goto cmd_parse_err;
			  }
			*v = '\0';

			if (!id_is_valid(argv[i]))
			  {
				PRINTF_ERROR("invalid port identifier '%s'",
					     argv[i]);
				goto cmd_parse_err;
			  }

			float val;

			if (!strtof_nl(&val, ++v))
			  {
				PRINTF_ERROR("invalid value for port '%s'",
					     argv[i]);
				goto cmd_parse_err;
			  }

			connection_t *tmp = cmd_connection_get_by_symbol(
						argv[i]);
			if (tmp != NULL)
				if (tmp->value != val)
				  {
					PRINTF_ERROR("a different value for "
						     "port '%s' was already "
						     "specified", argv[i]);
					goto cmd_parse_err;
				  }

			connections_n++;
			tmp = realloc(connections,
				      connections_n * sizeof(connection_t));
			if (tmp == NULL)
			  {
				PRINTF_ERROR("%s", strerror(ENOMEM));
				goto cmd_parse_err;
			  }
			connections = tmp;

			connections[connections_n - 1].symbol = argv[i];
			connections[connections_n - 1].value = val;
			connections[connections_n - 1].port = NULL;

			continue;
		  }

		if (!strcmp(argv[i], "--with-latency"))
		  {
			latency_do = false;
			continue;
		  }

		if (!strcmp(argv[i], "--timing"))
		  {
			timing_do = true;
			continue;
		  }

		if (!strcmp(argv[i], "--normalize"))
		  {
			normalize_do = true;
			continue;
		  }

		if (argv[i][0] == '-')
		  {
			PRINTF_ERROR("unknown command line option '%s'",
				     argv[i]);
			goto cmd_parse_err;
		  }

		if (plugin_uri != NULL)
		  {
			PRINT_ERROR("plugin URI specified more than once");
			goto cmd_parse_err;
		  }

		plugin_uri = argv[i];
	  }

	if (in_filename == NULL)
	  {
		PRINT_ERROR("no input file specified");
		goto cmd_parse_err;
	  }

	if (out_filename == NULL)
	  {
		PRINT_ERROR("no output file specified");
		goto cmd_parse_err;
	  }

	if (plugin_uri == NULL)
	  {
		PRINT_ERROR("no plugin URI specified");
		goto cmd_parse_err;
	  }

	if (frames_n == 0)
		frames_n = FRAMES_N_DEFAULT;

	return 0;

cmd_parse_err:
	if (connections != NULL)
		free(connections);

	return -1;
}

static void
cmd_fini()
{
	if (connections != NULL)
		free(connections);
}

/* LV2 */

static LilvWorld *lv2_world;
static const LilvPlugins *lv2_plugins;
static LilvNode *lv2_input_node, *lv2_output_node, *lv2_audio_node,
		*lv2_ctrl_node, *lv2_conn_opt_node, *lv2_rep_lat_node,
		*lv2_sample_rate_node;
static const LilvPort *lv2_latency_port = NULL;

static uint32_t audio_in_min = 0, audio_in_max = 0, audio_in_n, audio_out_n = 0,
		ctrl_in_n = 0, ctrl_out_n = 0;

static int
lv2_init()
{
	lv2_world = lilv_world_new();
	if (lv2_world == NULL)
	  {
		PRINT_ERROR("could not initialize Lilv");
		return 0;
	  }

	lilv_world_load_all(lv2_world);

	lv2_plugins = lilv_world_get_all_plugins(lv2_world);

	lv2_input_node = lilv_new_uri(lv2_world, LILV_URI_INPUT_PORT);
	lv2_output_node = lilv_new_uri(lv2_world, LILV_URI_OUTPUT_PORT);
	lv2_audio_node = lilv_new_uri(lv2_world, LILV_URI_AUDIO_PORT);
	lv2_ctrl_node = lilv_new_uri(lv2_world, LILV_URI_CONTROL_PORT);
	lv2_conn_opt_node = lilv_new_uri(lv2_world,
					 LILV_NS_LV2 "connectionOptional");
	lv2_rep_lat_node = lilv_new_uri(lv2_world,
					LILV_NS_LV2 "reportsLatency");
	lv2_sample_rate_node = lilv_new_uri(lv2_world,
					    LILV_NS_LV2 "sampleRate");

	return 1;
}

static const LilvPlugin *
lv2_plugin_get(const char *uri)
{
	LilvNode *uri_node;
	const LilvPlugin *plugin;

	uri_node = lilv_new_uri(lv2_world, uri);
	plugin = lilv_plugins_get_by_uri(lv2_plugins, uri_node);
	lilv_node_free(uri_node);

	return plugin;
}

static bool
lv2_plugin_check_features(const LilvPlugin *plugin)
{
	LilvNodes *features;
	bool ret = true;

	features = lilv_plugin_get_required_features(plugin);
	if (lilv_nodes_size(features))
		ret = false;
	lilv_nodes_free(features);

	return ret;
}

static bool
lv2_plugin_inspect_ports(const LilvPlugin *plugin)
{
	uint32_t n = lilv_plugin_get_num_ports(plugin);

	for (uint32_t i = 0; i < n; i++)
	  {
		const LilvPort *port = lilv_plugin_get_port_by_index(plugin, i);
		const char *symbol = lilv_node_as_string(
					lilv_port_get_symbol(plugin, port));

		if (lilv_port_is_a(plugin, port, lv2_audio_node))
		  {
			if (lilv_port_is_a(plugin, port, lv2_input_node))
			  {
				if (cmd_connection_get_by_symbol(symbol)
				    != NULL)
				  {
					PRINTF_ERROR("cannot assign a value to "
						     "port '%s' (it is an "
						     "audio in port)", symbol);
					return false;
				  }

				audio_in_max++;
				if (!lilv_port_has_property(plugin, port,
							    lv2_conn_opt_node))
					audio_in_min++;
			  }
			else if (lilv_port_is_a(plugin, port, lv2_output_node))
			  {
				if (cmd_connection_get_by_symbol(symbol)
				    != NULL)
				  {
					PRINTF_ERROR("cannot assign a value to "
						     "port '%s' (it is an "
						     "audio out port)", symbol);
					return false;
				  }

				if (!lilv_port_has_property(plugin, port,
							    lv2_conn_opt_node))
					audio_out_n++;
			  }
			else
			  {
				PRINTF_ERROR("port '%s' has a port type this "
					     "program does not understand",
					     symbol);
				return false;
			  }
		  }
		else if (lilv_port_is_a(plugin, port, lv2_ctrl_node))
		  {
			if (lilv_port_is_a(plugin, port, lv2_input_node))
			  {
				connection_t *conn =
					cmd_connection_get_by_symbol(symbol);

				if (conn != NULL)
				  {
					conn->port = port;
					ctrl_in_n++;
				  }
				else if (!lilv_port_has_property(plugin, port,
							lv2_conn_opt_node))
					ctrl_in_n++;
			  }
			else if (lilv_port_is_a(plugin, port, lv2_output_node))
			  {
				if (cmd_connection_get_by_symbol(symbol)
				    != NULL)
				  {
					PRINTF_ERROR("cannot assign a value to "
						     "port '%s' (it is a "
						     "control out port)",
						     symbol);
					return false;
				  }

				if (latency_do && lilv_port_is_a(plugin, port,
							lv2_rep_lat_node))
				  {
					lv2_latency_port = port;
					ctrl_out_n++;
				  }
				else if (!lilv_port_has_property(plugin, port,
							lv2_conn_opt_node))
					ctrl_out_n++;
			  }
			else
			  {
				PRINTF_ERROR("port '%s' has a port type this "
					     "program does not understand",
					     symbol);
				return false;
			  }
		  }
		else
		  {
			PRINTF_ERROR("port '%s' has a port type this program "
				     "does not understand", symbol);
			return false;
		  }
	  }

	for (size_t i = 0; i < connections_n; i++)
		if (connections[i].port == NULL)
		  {
			PRINTF_ERROR("port '%s' not found",
				     connections[i].symbol);
			return false;
		  }

	if (audio_in_max == 0)
	  {
		PRINT_ERROR("the plugin has no audio in ports");
		return false;
	  }

	if (audio_out_n == 0)
	  {
		PRINT_ERROR("the plugin has no audio out ports");
		return false;
	  }

	return true;
}

static void
lv2_fini()
{
	lilv_node_free(lv2_sample_rate_node);
	lilv_node_free(lv2_rep_lat_node);
	lilv_node_free(lv2_conn_opt_node);
	lilv_node_free(lv2_ctrl_node);
	lilv_node_free(lv2_audio_node);
	lilv_node_free(lv2_output_node);
	lilv_node_free(lv2_input_node);

	lilv_world_free(lv2_world);
}

/* Sound processing */

static float *in_buf, *out_buf;
static float **audio_in_bufs, **audio_out_bufs;
static float *ctrl_in_values = NULL, *ctrl_out_values = NULL;
static float **norm_bufs = NULL;
static float latency;

static bool
proc_bufs_alloc(sf_count_t frames)
{
	in_buf = malloc(audio_in_n * frames_n * sizeof(float));
	if (in_buf == NULL)
		goto in_err;

	out_buf = malloc(audio_out_n * frames_n * sizeof(float));
	if (out_buf == NULL)
		goto out_err;

	audio_in_bufs = malloc(audio_in_n * sizeof(float *));
	if (audio_in_bufs == NULL)
		goto audio_in_err;

	for (uint32_t i = 0; i < audio_in_n; i++)
	  {
		audio_in_bufs[i] = malloc(frames_n * sizeof(float));
		if (audio_in_bufs[i] == NULL)
		  {
			for (uint32_t j = 0; j < i; j++)
				free(audio_in_bufs[j]);
			free(audio_in_bufs);
			goto audio_in_err;
		  }
	  }

	audio_out_bufs = malloc(audio_out_n * sizeof(float *));
	if (audio_out_bufs == NULL)
		goto audio_out_err;

	for (uint32_t i = 0; i < audio_out_n; i++)
	  {
		audio_out_bufs[i] = malloc(frames_n * sizeof(float));
		if (audio_out_bufs[i] == NULL)
		  {
			for (uint32_t j = 0; j < i; j++)
				free(audio_out_bufs[j]);
			free(audio_out_bufs);
			goto audio_out_err;
		  }
	  }

	if (ctrl_in_n > connections_n)
	  {
		ctrl_in_values = malloc((ctrl_in_n - connections_n)
					* sizeof(float));
		if (ctrl_in_values == NULL)
			goto ctrl_in_err;
	  }

	if (ctrl_out_n > 0)
	  {
		ctrl_out_values = malloc(ctrl_out_n * sizeof(float));
		if (ctrl_out_values == NULL)
			goto ctrl_out_err;
	  }

	if (normalize_do)
	  {
		norm_bufs = malloc(audio_out_n * sizeof(float *));
		if (norm_bufs == NULL)
			goto norm_err;
		for (uint32_t i = 0; i < audio_out_n; i++)
		  {
			norm_bufs[i] = malloc(frames * sizeof(float));
			if (norm_bufs[i] == NULL)
			  {
				for (uint32_t j = 0; j < i; j++)
					free(norm_bufs[j]);
				free(norm_bufs);
				goto norm_err;
			  }
		  }
	  }

	return true;

norm_err:
	if (ctrl_out_values != NULL)
		free(ctrl_out_values);
ctrl_out_err:
	if (ctrl_in_values != NULL)
		free(ctrl_in_values);
ctrl_in_err:
	for (uint32_t i = 0; i < audio_out_n; i++)
		free(audio_out_bufs[i]);
	free(audio_out_bufs);
audio_out_err:
	for (uint32_t i = 0; i < audio_in_n; i++)
		free(audio_in_bufs[i]);
	free(audio_in_bufs);
audio_in_err:
	free(out_buf);
out_err:
	free(in_buf);
in_err:
	return false;
}

static void
proc_bufs_connect(const LilvPlugin *plugin, LilvInstance *instance,
		  double sample_rate)
{
	uint32_t audio_in_i = 0, audio_out_i = 0, ctrl_in_i = 0, ctrl_out_i = 0;

	uint32_t n = lilv_plugin_get_num_ports(plugin);
	for (uint32_t i = 0; i < n; i++)
	  {
		const LilvPort *port = lilv_plugin_get_port_by_index(plugin, i);

		if (lilv_port_is_a(plugin, port, lv2_audio_node))
		  {
			if (lilv_port_has_property(plugin, port,
						   lv2_conn_opt_node))
				continue;

			if (lilv_port_is_a(plugin, port, lv2_input_node))
			  {
				lilv_instance_connect_port(instance, i,
						audio_in_bufs[audio_in_i]);
				audio_in_i++;
			  }
			else
			  {
				lilv_instance_connect_port(instance, i,
						audio_out_bufs[audio_out_i]);
				audio_out_i++;
			  }
		  }
		else if (lilv_port_is_a(plugin, port, lv2_input_node))
		  {
			connection_t *conn = cmd_connection_get_by_port(port);

			if ((conn == NULL)
			    && lilv_port_has_property(plugin, port,
						      lv2_conn_opt_node))
				continue;

			LilvNode *min, *max, *deflt;
			lilv_port_get_range(plugin, port, &deflt, &min, &max);

			if (conn != NULL)
			  {
				if ((min != NULL) && (max != NULL))
				  {
					double amin = lilv_node_as_float(min);
					double amax = lilv_node_as_float(max);

					if (lilv_port_has_property(plugin, port,
						lv2_sample_rate_node))
					  {
						amin *= sample_rate;
						amax *= sample_rate;
					  }

					if ((conn->value < amin)
					    || (conn->value > amax))
						PRINTF_WARNING("out of range "
							"value for port '%s' "
							"(min: %g, max: %g)",
							lilv_node_as_string(
							  lilv_port_get_symbol(
							    plugin, port)),
							amin, amax);
				  }
				else if (min != NULL)
				  {
					double amin = lilv_node_as_float(min);

					if (lilv_port_has_property(plugin, port,
						lv2_sample_rate_node))
						amin *= sample_rate;

					if (conn->value < amin)
						PRINTF_WARNING("out of range "
							"value for port '%s' "
							"(min: %g)",
							lilv_node_as_string(
							  lilv_port_get_symbol(
							    plugin, port)),
							amin);
				  }
				else if (max != NULL)
				  {
					double amax = lilv_node_as_float(max);

					if (lilv_port_has_property(plugin, port,
						lv2_sample_rate_node))
						amax *= sample_rate;

					if (conn->value > amax)
						PRINTF_WARNING("out of range "
							"value for port '%s' "
							"(max: %g)",
							lilv_node_as_string(
							  lilv_port_get_symbol(
							    plugin, port)),
							amax);
				  }

				lilv_instance_connect_port(instance, i,
							   &conn->value);
			  }
			else
			  {
				if (deflt != NULL)
					ctrl_in_values[ctrl_in_i] =
						lilv_node_as_float(deflt);
				else if (min != NULL)
					ctrl_in_values[ctrl_in_i] =
						lilv_node_as_float(min);
				else if (max != NULL)
					ctrl_in_values[ctrl_in_i] =
						lilv_node_as_float(max);
				else
					ctrl_in_values[ctrl_in_i] = 0.0;

				lilv_instance_connect_port(instance, i,
							   ctrl_in_values
							   + ctrl_in_i);
				ctrl_in_i++;
			  }

			if (deflt != NULL)
				lilv_node_free(deflt);
			if (min != NULL)
				lilv_node_free(min);
			if (max != NULL)
				lilv_node_free(max);
		  }
		else
		  {
			if (port == lv2_latency_port)
				lilv_instance_connect_port(instance, i,
							   &latency);
			else if (!lilv_port_has_property(plugin, port,
							 lv2_conn_opt_node))
			  {
				lilv_instance_connect_port(instance, i,
						ctrl_out_values + ctrl_out_i);
				ctrl_out_i++;
			  }
		  }
	  }

	if (audio_in_i == audio_in_n)
		return;

	n = lilv_plugin_get_num_ports(plugin);
	for (uint32_t i = 0; (i < n) && (audio_in_i < audio_in_n); i++)
	  {
		const LilvPort *port = lilv_plugin_get_port_by_index(plugin, i);

		if (!lilv_port_has_property(plugin, port, lv2_conn_opt_node))
			continue;

		if (!lilv_port_is_a(plugin, port, lv2_audio_node))
			continue;

		if (!lilv_port_is_a(plugin, port, lv2_input_node))
			continue;

		lilv_instance_connect_port(instance, i,
					   audio_out_bufs[audio_in_i]);
		audio_in_i++;
	  }
}

static void
proc_demux_in(sf_count_t frames)
{
	for (sf_count_t i = 0; i < frames; i++)
		for (uint32_t j = 0; j < audio_in_n; j++)
			audio_in_bufs[j][i] = in_buf[i * audio_in_n + j];
}

static void
proc_mux_out(float **src_bufs, sf_count_t frames, sf_count_t in_offset,
	     sf_count_t out_offset)
{
	for (sf_count_t i = out_offset; i < frames; i++)
		for (uint32_t j = 0; j < audio_out_n; j++)
			out_buf[(i - out_offset) * audio_out_n + j] =
				src_bufs[j][i + in_offset];
}

static void
proc_out_checks(sf_count_t frames, bool *nan_warning, bool *inf_warning,
		bool *clip_warning, float *out_max)
{
	for (uint32_t i = 0; i < audio_out_n; i++)
		for (sf_count_t j = 0; j < frames; j++)
		  {
			if (isnan(audio_out_bufs[i][j]))
			  {
				if (!*nan_warning)
				  {
					PRINT_WARNING("NaN detected in audio "
						      "output");
					*nan_warning = true;
				  }
				audio_out_bufs[i][j] = 0.0;
			  }
			else if (isinf(audio_out_bufs[i][j]))
			  {
				if (!*inf_warning)
				  {
					PRINT_WARNING("infinity value detected "
						      "in audio output");
					*inf_warning = true;
				  }

				if (normalize_do)
					continue;
			  }

			if (normalize_do)
			  {
				float aval = fabsf(audio_out_bufs[i][j]);
				if (aval > *out_max)
					*out_max = aval;

				continue;
			  }

			if (audio_out_bufs[i][j] < -1.0)
			  {
				if (!*clip_warning)
				  {
					PRINT_WARNING("out of range value "
						      "detected in audio "
						      "output, clipping");
					*clip_warning = true;
				  }
				audio_out_bufs[i][j] = -1.0;
			  }
			else if (audio_out_bufs[i][j] > 1.0)
			  {
				if (!*clip_warning)
				  {
					PRINT_WARNING("out of range value "
						      "detected in audio "
						      "output, clipping");
					*clip_warning = true;
				  }
				audio_out_bufs[i][j] = 1.0;
			  }
		  }
}

static void
proc_run(SNDFILE *in, SNDFILE *out, LilvInstance *instance)
{
	sf_count_t frames, out_offset, norm_i;
	float latency_left = -1.0;
	bool nan_warning = false, inf_warning = false, clip_warning = false;
	clock_t total_time = 0, process_time = 0, pre_time = 0; // gcc happy
	float out_max = 0.0;

	if (timing_do)
		total_time = clock();

	if (normalize_do)
		norm_i = 0;

	while ((frames = sf_readf_float(in, in_buf, frames_n)) > 0)
	  {
		out_offset = 0;

		proc_demux_in(frames);

		if (timing_do)
			pre_time = clock();

		lilv_instance_run(instance, frames);

		if (timing_do)
			process_time += clock() - pre_time;

		proc_out_checks(frames, &nan_warning, &inf_warning,
				&clip_warning, &out_max);

		// Compensate latency
		if (lv2_latency_port != NULL)
		  {
			if (latency_left < 0.0)
				latency_left = latency;
			if (latency_left > 0.0)
			  {
				if (latency_left >= (float)frames)
				  {
					latency_left -= (float)frames;
					continue;
				  }
				out_offset = roundf(latency_left);
				latency_left = 0.0;
			  }
		  }

		if (normalize_do)
		  {
			for (uint32_t i = 0; i < audio_out_n; i++)
				memcpy(norm_bufs[i] + norm_i, audio_out_bufs[i],
				       (frames - out_offset) * sizeof(float));
			norm_i += frames - out_offset;
		  }
		else
		  {
			proc_mux_out(audio_out_bufs, frames, 0, out_offset);
			sf_writef_float(out, out_buf, (frames - out_offset));
		  }
	  }

	// Extra silence processing for latency compensation
	if (lv2_latency_port != NULL)
	  {
		out_offset = roundf(latency);
		while ((frames = MIN(frames_n, out_offset)) > 0)
		  {
			proc_demux_in(frames);

			if (timing_do)
				pre_time = clock();

			lilv_instance_run(instance, frames);

			if (timing_do)
				process_time += clock() - pre_time;

			proc_out_checks(frames, &nan_warning, &inf_warning,
					&clip_warning, &out_max);

			if (normalize_do)
			  {
				for (uint32_t i = 0; i < audio_out_n; i++)
					memcpy(norm_bufs[i] + norm_i,
					       audio_out_bufs[i],
					       frames * sizeof(float));
				norm_i += frames;
			  }
			else
			  {
				proc_mux_out(audio_out_bufs, frames, 0, 0);
				sf_writef_float(out, out_buf, frames);
			  }

			out_offset -= frames;
		  }
	  }

	if (normalize_do)
	  {
		if (out_max == 0.0)
		  {
			PRINT_WARNING("the plugin produced silent output, "
				      "cannot normalize");
		  }
		else
		  {
			out_max = 1 / out_max;
			printf("Applying %gx gain (%g dB) to all output "
			       "channels\n", out_max, 20.0 * logf(out_max));

			for (uint32_t i = 0; i < audio_out_n; i++)
				for (sf_count_t j = 0; j < norm_i; j++)
					if (!isinf(norm_bufs[i][j]))
						norm_bufs[i][j] *= out_max;
					else if (signbit(norm_bufs[i][j]))
						norm_bufs[i][j] = -1.0;
					else
						norm_bufs[i][j] = 1.0;
		  }

		sf_count_t i = 0;
		while ((frames = MIN(frames_n, norm_i - i)) > 0)
		  {
			proc_mux_out(norm_bufs, frames, i, 0);
			sf_writef_float(out, out_buf, frames);
			i += frames;
		  }
	  }

	if (timing_do)
	  {
		total_time = clock() - total_time;
		printf("Total time     : %g s\n",
		       (double)total_time / CLOCKS_PER_SEC);
		printf("Processing time: %g s\n",
		       (double)process_time / CLOCKS_PER_SEC);
	  }
}

static void
proc_bufs_free()
{
	free(in_buf);
	free(out_buf);

	for (uint32_t i = 0; i < audio_in_n; i++)
		free(audio_in_bufs[i]);
	free(audio_in_bufs);

	for (uint32_t i = 0; i < audio_out_n; i++)
		free(audio_out_bufs[i]);
	free(audio_out_bufs);

	if (ctrl_in_values != NULL)
		free(ctrl_in_values);

	if (ctrl_out_values != NULL)
		free(ctrl_out_values);

	if (norm_bufs != NULL)
		free(norm_bufs);
}

/* Main */

int
main(int argc, char *argv[])
{
	int exit_code = EXIT_SUCCESS;

	argv0 = argv[0];

	int err = cmd_parse(argc, argv);
	if (err > 0)
		return EXIT_SUCCESS;
	else if (err < 0)
		return EXIT_FAILURE;

	if (!lv2_init())
	  {
		exit_code = EXIT_FAILURE;
		goto lv2_init_err;
	  }

	const LilvPlugin *plugin = lv2_plugin_get(plugin_uri);
	if (plugin == NULL)
	  {
		PRINTF_ERROR("plugin '%s' not found", plugin_uri);
		exit_code = EXIT_FAILURE;
		goto lv2_plugin_err;
	  }

	if (!lv2_plugin_check_features(plugin))
	  {
		PRINT_ERROR("the plugin requires features that this program "
			    "does not support, sorry");
		exit_code = EXIT_FAILURE;
		goto lv2_plugin_err;
	  }

	if (!lv2_plugin_inspect_ports(plugin))
	  {
		exit_code = EXIT_FAILURE;
		goto lv2_plugin_err;
	  }

	SNDFILE *in;
	SF_INFO in_info;

	in_info.format = 0;
	in = sf_open(in_filename, SFM_READ, &in_info);
	if (in == NULL)
	  {
		PRINTF_ERROR("could not open input file: %s",
			     sf_strerror(NULL));
		exit_code = EXIT_FAILURE;
		goto in_open_err;
	  }

	if ((in_info.channels < audio_in_min)
	    || (in_info.channels > audio_in_max))
	  {
		PRINTF_ERROR("the number of channels of the input file (%d) "
			     "and the number of input ports of the plugin (%"
			     PRIu32 " to %" PRIu32 ") do not match",
			     in_info.channels, audio_in_min, audio_in_max);
		exit_code = EXIT_FAILURE;
		goto in_channels_err;
	  }

	audio_in_n = in_info.channels;

	LilvInstance *instance = lilv_plugin_instantiate(plugin,
							 in_info.samplerate,
							 NULL);
	if (instance == NULL)
	  {
		PRINT_ERROR("failed to instantiate the plugin");
		exit_code = EXIT_FAILURE;
		goto instantiate_err;
	  }

	if (!proc_bufs_alloc(in_info.frames))
	  {
		PRINTF_ERROR("%s", strerror(ENOMEM));
		exit_code = EXIT_FAILURE;
		goto bufs_err;
	  }

	SNDFILE *out;
	SF_INFO out_info;

	out_info.samplerate = in_info.samplerate;
	out_info.channels = audio_out_n;
	out_info.format = in_info.format;

	out = sf_open(out_filename, SFM_WRITE, &out_info);
	if (out == NULL)
	  {
		PRINTF_ERROR("could not open output file: %s",
			     sf_strerror(NULL));
		exit_code = EXIT_FAILURE;
		goto out_open_err;
	  }

	proc_bufs_connect(plugin, instance, in_info.samplerate);

	lilv_instance_activate(instance);

	proc_run(in, out, instance);

	lilv_instance_deactivate(instance);

	if (sf_close(out) != 0)
		PRINTF_WARNING("could not close output file: %s",
			       sf_strerror(out));

out_open_err:
	proc_bufs_free();

bufs_err:
	lilv_instance_free(instance);

instantiate_err:
in_channels_err:
	if (sf_close(in) != 0)
		PRINTF_WARNING("could not close input file: %s",
			       sf_strerror(in));

in_open_err:
lv2_plugin_err:
	lv2_fini();

lv2_init_err:
	cmd_fini();

	return exit_code;
}
